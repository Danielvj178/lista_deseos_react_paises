import React, { Component } from "react";
import Header from "./prueba";
import Options from "./Options";
import AddOptions from "./AddOptions";

class WishList extends Component
{
    constructor(props)   
    {
        super(props);
        this.state = {options:[]};
        this.handleAddOption = this.handleAddOption.bind(this);
        this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
    }
    handleAddOption(option)
    {
        this.setState((prevState) => {
          return {options: prevState.options.concat(option)}
        });
    }
    handleDeleteOptions(option)
    {
        const index = this.state.options.indexOf(option);
        alert(option);
        this.setState((prevState) => {
            return  prevState.options.splice(index,1)
        })
    }
    render() {
        return (
            <div>
                <Options options = {this.state.options} handleDeleteOptions={this.handleDeleteOptions}/>
                <AddOptions handleAddOption={this.handleAddOption}/>
            </div>
        );
    }
}
export default WishList;


import React, { Component }  from "react";
import {Button, Icon, Input, Row} from 'react-materialize'


class AddOption  extends Component
{
    constructor(props)
    {
        super(props);
        this.handleAddOption = this.handleAddOption.bind(this);
        this.state =
            {
                error: undefined
            };
    }
    handleAddOption(e)
    {
        e.preventDefault();
        const option = e.target.elements.option.value.trim();
        const error = this.props.handleAddOption(option);
        this.setState(() => {
            return { error };
        });
    }
    render()
    {
        return (
            <div>
                {this.state.error && <p>{this.state.error}</p>}
                <form onSubmit={this.handleAddOption}>
                <Input placeholder="Placeholder" s={4}  offset="2" label="First Name" name="option" type="text"/>
                <Button waves='light'>Add Option<Icon left>cloud</Icon></Button>
                </form>
            </div>
        );
    }

}

export default AddOption;

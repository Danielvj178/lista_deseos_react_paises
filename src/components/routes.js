import React from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import { Navbar, NavItem, Icon } from 'react-materialize';
import WishList from './WishList';
import Header from './prueba';
import Countries from './countries';
const Menu = () =>
    (
        <Router>
            <div>
                <Navbar brand='logo' right>
                    <NavItem>
                        <Link to="/"><Icon>search</Icon></Link></NavItem>
                    <NavItem >
                        <Link to="/countries"><Icon>view_modul</Icon></Link> </NavItem>
                    <NavItem href='get-started.html'><Icon>refresh</Icon></NavItem>
                    <NavItem href='get-started.html'><Icon>more_vert</Icon></NavItem>
                </Navbar>
                <Header title="Pueba state react" subtitle="Reloj" />
                <Route exact path="/" component={WishList} />
                <Route exact path="/countries" component={Countries} />
            </div>
        </Router>


    );
export default Menu;
import React, { Component } from 'react';

    class Reloj extends Component
    {
        constructor(props)
        {
            super(props);
            this.state = {date: new Date()};
            this.tick.bind(this);
        }

        componentDidMount()
        {
            this.timeID = setInterval(() => this.tick(),1000);
        }
        componentWillUnmount()
        {
            clearInterval(this.timeID);
        }
        render()
        {
            return(
                <div>
                    <p>{this.state.date.toLocaleTimeString()}</p>
                </div>
            );
        }
        tick()
        {
            this.setState(
                {
                    date: new Date()
                }
            );
        }
    }
export default  Reloj;
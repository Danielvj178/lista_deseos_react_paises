import React, { Component } from 'react';
import Emoticones from './emoticones';

    class Header extends Component
    {
        constructor(props)
        {
            super(props);
            this.state = {date: new Date()};
            this.tick.bind(this);
        }

        componentDidMount()
        {
            this.timeID = setInterval(() => this.tick(),1000);
        }
        componentWillUnmount()
        {
            clearInterval(this.timeID);
        }
        render()
        {
            return(
                <div>
                    <h1>{this.props.title}</h1>
                    <h4>{this.props.subtitle}</h4>
                    <p>La hora es :{this.state.date.toLocaleTimeString()}</p>
                    <Emoticones hora={this.state.date.getSeconds()}  />
                </div>
            );
        }
        tick()
        {
            this.setState(
                {
                    date: new Date()
                }
            );
        }
    }
export default  Header;
import React, { Component } from 'react';
import { FaBeer, FaFolder } from 'react-icons/fa';

const Emoticon =  (props) => 
{
    return (
    <div>
        <p>{(props.hora >= 20 && props.hora <= 40 ) ? <FaBeer /> : <FaFolder />}</p>
    </div>)
}

export default Emoticon;
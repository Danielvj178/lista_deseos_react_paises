import React ,{ Component } from "react";
import { Input ,Button,Row} from "react-materialize";
import  Country  from './country';
class Countries extends Component
{
    constructor(props)
    {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {pais:[]}
    }
    handleSubmit(e)
    {
        e.preventDefault();
        const name = e.target.elements.pais.value.trim();
        if(name)
        {
        fetch(`https://restcountries.eu/rest/v2/name/${name}`)
         .then((response) => {
                return response.json();
        })
        .then((dataJson) => 
        {
            if(dataJson)
            {
                this.setState({pais:dataJson});
            }
        })
        .catch((error) => 
        {
            console.log("error" + error.message)
        });
        }
    }
    render()
    {
        return(
            <div>
                <form onSubmit={this.handleSubmit}>
                <Row>
                    <Input placeholder="Pais" name="pais" s={4} type="text"/>
                    <Button s={2} floating large className='red' waves='light' icon='search' />
                </Row>
                </form> 
                    {this.state.pais.map((pais) => {return <Country key={pais.numericCode}  country={pais}/>})}
            </div>
        );
    }
    
}

export default Countries;
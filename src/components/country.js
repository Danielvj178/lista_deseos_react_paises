import React , { Component } from 'react';
class Country extends Component
{
    render()
    {
        return(
            <div>
                <p>Pais:{this.props.country.nativeName}</p>
                <p>subRegion:{this.props.country.subregion}</p>
                <p>Lenguaje:{this.props.country.languages.nativeLanguage}</p>
                <img src={this.props.country.flag} alt="imagen"/>
            </div>
        );
    }
}
export default Country;


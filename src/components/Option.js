import React, { Component } from "react";
import  { FaTimesCircle, FaBeer } from "react-icons/fa";
import Reloj from './reloj';

class Option extends Component
{
    constructor(props)
    {
        super(props);
        this.handleDeleteOption = this.handleDeleteOption.bind(this);
    }
    handleDeleteOption()
    {
        this.props.handleDeleteOptions(this.props.optionText);
    }
    handleIcon()
    {
        if(this.props.optionText.toLowerCase() == "cerveza")
        {
            return  <div>{"Cervezaaa!"}<FaBeer/></div>
        }else if(this.props.optionText.toLowerCase() == "hora")
        {
            return <Reloj/>
        }else
        {
            return this.props.optionText;
        }
    }
    render()
    {
        return(
            <div>
              { this.handleIcon() }<span onClick={this.handleDeleteOption}><FaTimesCircle/></span>
            </div>
        );
    }
}
export default Option;
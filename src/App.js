import React, { Component } from 'react';
import Header from './components/prueba';
import WishList from './components/WishList';
import Menu from './components/routes';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Menu />
      </div>
    );
  }
}

export default App;
